var expect = require("chai").expect;
var matches = require("./matches_per_year");
var fs = require('fs');
var csv = require('fast-csv');

describe("matches", function () {
	it("should return total number of matches per team", function () {
		var totalMatchPerYear = [
			2008,
			2008,
			2009,
			2009,
			2010,
			2010,
			2010,
			2018
		]

		var expectedResult = [
			[2008, 2],
			[2009, 2],
			[2010, 3],
			[2018, 1]
		]
		var a = matches.getTotalMatchesPerYear(totalMatchPerYear);
		expect(matches.getTotalMatchesPerYear(totalMatchPerYear)).deep.equals(expectedResult);
	})
});
