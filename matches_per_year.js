

function getTotalMatchesPerYear(totalMatchPerYear){
	let arr = [];
	let match = 1;
	for (let i = 0; i < totalMatchPerYear.length; i++) {
		if(totalMatchPerYear[i]==totalMatchPerYear[i+1]){
			match++;
		}else{
			arr.push([totalMatchPerYear[i],match]);
			match =1;
		}
	}
	return arr;
}

module.exports ={
	getTotalMatchesPerYear : getTotalMatchesPerYear
}