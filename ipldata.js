
function getTotalMatchPerYear() {
    var fs = require('fs');
    var csv = require('fast-csv');

    var totalMatchPerYear = [];
    fs.createReadStream('matches.csv').pipe(csv()).on('data', function (data) {
        if (Number(data[1])) {
            totalMatchPerYear.push(data[1]);
        }
    }).on('end', function () {
        let seasonArray = [];
        let totalMatch = 1;
        for (let i = 0; i < totalMatchPerYear.length; i++) {
            if (totalMatchPerYear[i] == totalMatchPerYear[i + 1]) {
                totalMatch++;
            } else {
                seasonArray.push([totalMatchPerYear[i], totalMatch]);
                totalMatch = 1;
            }
        }
        console.log(seasonArray);
        fs.writeFile('matchesPerYear.json', JSON.stringify(seasonArray), (err) => {
            if (err) throw err
            console.log('The file has been saved!')
        })

    });
}

// getTotalMatchPerYear();


function matchWinnerPerYear() {
    var fs = require('fs');
    var csv = require('fast-csv');

    let winner = [];
    let teams = new Set();
    let winnersPerYear = [];
    fs.createReadStream('matches.csv').pipe(csv()).on('data', function (data) {
        if (data[10] !== "" && data[10] !== "winner")
            teams.add(data[10]);

        let obj = {};
        obj.season = data[1];
        obj.winner = data[10];
        winnersPerYear.push(obj);

    }).on('end', function () {
        for (let team of teams) {
            let count = 0;
            for (let index = 1; index < winnersPerYear.length - 1; index++) {
                if (index === winnersPerYear.length - 2) {
                    count++;
                } else if (winnersPerYear[index].season === winnersPerYear[index + 1].season) {
                    if (team === winnersPerYear[index].winner)
                        count++;
                } else {

                    let obj = {};
                    obj.team = team;
                    obj.year = winnersPerYear[index].season;
                    obj.win = count;
                    winner.push(obj);
                    count = 0;

                }
            }
            let obj = {};
            obj.team = team;
            obj.year = winnersPerYear[winnersPerYear.length - 1].season;
            obj.win = count;
            winner.push(obj);
        }
        console.log(winner);
    });
}
// matchWinnerPerYear();




function extraRunsConceded() {
    var fs = require('fs');
    var csv = require('fast-csv');
    let extraRuns = [];
    let matchID = [];
    let c = 0;
    let teams = new Set();
    fs.createReadStream('deliveries.csv').pipe(csv()).on('data', function (data) {

        if (c === 0) {
            fs.createReadStream('matches.csv').pipe(csv()).on('data', function (data) {
                if (data[1] == 2016) {
                    matchID.push(data[0]);
                }
            })
            c++;
        }
        if (matchID.includes(data[0])) {
            let obj = {};
            teams.add(data[2]);
            obj.match_id = data[0];
            obj.team = data[2];
            obj.extraRun = data[16];
            extraRuns.push(obj);
        }

    }).on('end', function () {
        let arr = [];
        let extras = 0;
        for (let team of teams) {
            for (let index = 0; index < extraRuns.length - 1; index++) {
                if (team === extraRuns[index].team) {
                    extras = extras + Number(extraRuns[index].extraRun);
                }
            }
            let obj = {};
            obj.team = team;
            obj.extra = extras;
            arr.push(obj);
            extras = 0;
        }
        console.log(arr);
    });
}
// extraRunsConceded();


function economicalBowler() {
    var fs = require('fs');
    var csv = require('fast-csv');
    let isChecked = true;
    fs.createReadStream('deliveries.csv').pipe(csv()).on('data', function (data) {
        if (isChecked === true) {
            fs.createReadStream('matches.csv').pipe(csv()).on('data', function (data) {
                if (data[1] == 2015) {
                    matchID.push(data[0]);
                }
            })
            isChecked = false;
        }

        if (matchID.includes(data[0])) {

        }

    }).on('end', function () {

    });
}
economicalBowler();






// var fs = require('fs');
// var csv = require('fast-csv');

// var matches = [];
// var totalMatchPerYear = [];

// fs.createReadStream('matches.csv').pipe(csv()).on('data', function (data) {
//     if (Number(data[1])) {
//         totalMatchPerYear.push(data[1]);
//     }
// }).on('end', function () {
//     // let jsonObj = JSON.stringify(matches, null, 4);
//     getTotalMatchPerYear(totalMatchPerYear);
// });

// //first question

// function getTotalMatchPerYear(totalMatchPerYear) {
//     var map = totalMatchPerYear.reduce(noOfMatchesPerYear, {});
//     function noOfMatchesPerYear(counter, year) {
//         counter[year] = ++counter[year] || 1;
//         return counter;
//     }
//     console.log(JSON.stringify(map));
// }

// // second question

// // console.log(teams);
// // // var winnersYearWise = matchWinnerPerYear();
// // console.log(winnersYearWise);
// // console.log(JSON.stringify(winnersYearWise));

// function matchWinnerPerYear() {
//     var winner = [];
//     for (let team of teams) {
//         let count = 0;
//         for (let index = 0; index < winnersPerYear.length - 1; index++) {
//             if (winnersPerYear[index].season == winnersPerYear[index + 1].season) {
//                 if(team==winnersPerYear[index].winner)
//                     count++;  
//             } else {

//                 let obj = {};
//                 obj.team = team;
//                 obj.year = winnersPerYear[index].season;
//                 obj.win = count;
//                 winner.push(obj);
//                 count=0;

//             }
//         }
//     }
//     return winner;
// }
// // module.exports = {
// // 	getTotalMatchPerYear: getTotalMatchPerYear
// // }

// // // var totalMatchPerYear = [];
// // // var winnersPerYear = [];
// // // var matchId = [];
// // var teams = new Set();
// // var seasons = new Set();
// // // var map = new Map();
// // var matchList = fs.readFileSync('matches.csv', 'utf8');
// // var matchRows = matchList.split('\n');
// // var matchFields = matchRows[0].split(',');

// // var deliveriList = fs.readFileSync('deliveries.csv', 'utf8');
// // var deliveriRows = deliveriList.split('\n');
// // var deliveriFields = deliveriRows[0].split(',');

// // for (let index = 1; index < matchRows.length; index++) {
// //     let fieldData = matchRows[index].split(',');

// //     if (index < matchRows.length - 1) {
// //         totalMatchPerYear.push(fieldData[1]);
// //     }


// //     let obj = {};
// //     if (fieldData[10]) {
// //         teams.add(fieldData[10]);
// //     }
// //     seasons.add(fieldData[1]);
// //     obj[matchFields[1]] = fieldData[1];
// //     obj[matchFields[10]] = fieldData[10];
// //     winnersPerYear.push(obj);

// //     if (fieldData[1] == 2016) {
// //         matchId.push(fieldData[0]);
// //     }
// // }



// // //second question

// // // // console.log(teams);
// // // var winnersYearWise = matchWinnerPerYear();
// // // // console.log(winnersYearWise);
// // // console.log(JSON.stringify(winnersYearWise));

// // // function matchWinnerPerYear() {
// // //     var winner = [];
// // //     for (let team of teams) {
// // //         let count = 0;
// // //         for (let index = 0; index < winnersPerYear.length - 1; index++) {
// // //             if (winnersPerYear[index].season == winnersPerYear[index + 1].season) {
// // //                 if(team==winnersPerYear[index].winner)
// // //                     count++;  
// // //             } else {

// // //                 let obj = {};
// // //                 obj.team = team;
// // //                 obj.year = winnersPerYear[index].season;
// // //                 obj.win = count;
// // //                 winner.push(obj);
// // //                 count=0;

// // //             }
// // //         }
// // //     }
// // //     return winner;
// // // }

// // var extraRuns = [];
// // var deliveriId = [];
// // for (let index = 1; index < deliveriList.length; index++) {
// //     let fieldData;
// //     if (deliveriRows[index]) {
// //         fieldData = deliveriRows[index].split(',');
// //         for (let index1 = 0; index1 < matchId.length; index1++) {
// //             if(fieldData[0]==matchId[index1]){
// //                 let obj ={};
// //                 obj[deliveriFields[0]] = fieldData[0];
// //                 obj[deliveriFields[16]] = fieldData[16];
// //                 deliveriId.push(fieldData[0]);
// //             }
// //         }


// //     }


// //     // deliveriId.push(fieldData[0]);

// // }

// // console.log(deliveriId);


// // for (let index = 0; index < matches.length; index++) {

// // console.log(matches.length);
// // }



// // var csvToJson = require('convert-csv-to-json');

// // var csvFile ='matches.csv';
// // // var jsonFile ='matches.json';

// // //  csvToJson.generateJsonFileFromCsv(csvFile,jsonFile);

// // let json = csvToJson.getJsonFromCsv("matches.csv");
// // json=csvToJson.formatValueByType().getJsonFromCsv(c);
