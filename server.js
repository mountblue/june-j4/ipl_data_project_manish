function loadDoc() {
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
      if (this.readyState == 4 && this.status == 200) {
       document.getElementById("demo").innerHTML = this.responseText;
      }
    };
    xhttp.open("GET", "matchesPerYear.json", true);
    xhttp.send();
  }

  function drawHighCharts(){

  Highcharts.chart('container', {
    chart: {
      type: 'column'
    },
    title: {
      text: 'MATCH PLAYED PER YEAR'
    },
    xAxis: {
      type: 'category',
      labels: {
        rotation: -45,
        style: {
          fontSize: '13px',
          fontFamily: 'Verdana, sans-serif'
        }
      }
    },
    yAxis: {
      min: 0,
      title: {
        text: 'Number of Match'
      }
    },
    legend: {
      enabled: false
    },
    tooltip: {
      pointFormat: 'Matches played per year<b>{point.y:.1f} </b>'
    },
    series: [{
      name: 'YEARS',
      data: ,
      dataLabels: {
        enabled: true,
        rotation: -90,
        color: '#FFFFFF',
        align: 'right',
        format: '{point.y:.1f}', // one decimal
        y: 10, // 10 pixels down from the top
        style: {
          fontSize: '13px',
          fontFamily: 'Verdana, sans-serif'
        }
      }
    }]
  });
  }